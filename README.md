# Axe Chat

[![Version](https://img.shields.io/badge/version-v0.1.0-blue.svg)](https://gitlab.com/s1rr/axe-chat/releases/tag/v0.1.0)
[![Author](https://img.shields.io/badge/author-s1rr-orange.svg)](https://gitlab.com/s1rr)


    (>|
      | A X E   C H A T
      ! Authentified with ECDSA chat on AX.25 protocol - v0.1.0 2023


## Table of Contents
- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## Description
Axe Chat is a Rust-based chat application that uses the AX.25 protocol for communication. It provides secure communication by authenticating messages using ECDSA.

## Installation
1. Install Rust: [Rust Installation Guide](https://www.rust-lang.org/tools/install)
2. Clone the repository: `git clone https://gitlab.com/s1rr/axe-chat.git`
3. Change into the project directory: `cd axe-chat`
4. Build the application: `cargo build --release`

## Usage
To run Axe Chat, use the following command:

```
$ ./target/release/axe-chat [command]
```

### Listen
Listen for incoming messages.

```
$ ./target/release/axe-chat listen <address>
```

### Send
Send a message to a name.

```
$ ./target/release/axe-chat send <name> <tnc> <message>
```

### Managing Keys
Axe Chat allows you to manage keys for secure communication.
```
$ ./target/release/axe-chat keys [command]
```

You can perform the following actions:
#### List
List keys for a specific name.

```
$ ./target/release/axe-chat keys list <name>
```

#### Add
Add a key for a specific name.

```
$ ./target/release/axe-chat keys add <name> <key>
```

#### Remove
Remove a key from a specific name.

```
$ ./target/release/axe-chat keys rem <name> <key>
```

#### Generate
Generate a new key for a specific name.

```
$ ./target/release/axe-chat keys gen <name>
```

#### Default
Set a default key for a specific name.

```
$ ./target/release/axe-chat keys default <name>
```

## License
This project is licensed under the GPLv3.0 License.

