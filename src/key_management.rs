use log::{error, info}; //debug, trace, warn
use openssl::{ec, pkey};
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead, BufReader, BufWriter, Write};
use std::path::Path;

pub struct MyKeypair {
    sk: Vec<u8>,
    pub pk: Vec<u8>,
}

pub struct KeyList {
    inner_map: HashMap<String, Vec<u8>>,
}

pub struct KeyListError(String);

impl std::fmt::Display for KeyListError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl std::fmt::Debug for KeyListError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl std::error::Error for KeyListError {}

impl KeyList {
    pub fn init() -> KeyList {
        KeyList {
            inner_map: HashMap::new(),
        }
    }

    pub fn add_key(
        &mut self,
        name: String,
        key: Vec<u8>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        if self.inner_map.contains_key(&name) {
            error!("Key already exists");
            Err(Box::new(KeyListError("Key already exists".to_string())))
        } else {
            info!(
                "key {} - {} successfully added",
                name,
                String::from_utf8_lossy(&key)
            );
            self.inner_map.insert(name, key);
            Ok(())
        }
    }

    pub fn rem_key(&mut self, name: String) -> Result<(), Box<dyn std::error::Error>> {
        if self.inner_map.contains_key(&name) {
            self.inner_map.remove(&name);
            info!("key {} successfully removed", name);
            Ok(())
        } else {
            error!("Key already exists");
            Err(Box::new(KeyListError("Key don't exist".to_string())))
        }
    }

    pub fn load_dictionary(&mut self, path: &Path) -> Result<(), Box<dyn std::error::Error>> {
        let display = path.display();
        let file = match File::open(path) {
            Err(ref error) if error.kind() == io::ErrorKind::NotFound => {
                match File::create(&path) {
                    Err(why) => {
                        error!("Error opening file {}: {}", display, why);
                        return Err(Box::new(why));
                    }
                    Ok(file) => file,
                }
            }
            Err(why) => {
                error!("couldn't open {}: {}", display, why);
                return Err(Box::new(why));
            }
            Ok(file) => file,
        };

        // iterate other the lines of the file
        let buf_reader = BufReader::new(file);

        for line in buf_reader.lines() {
            match line {
                Ok(line) => {
                    let mut parts = line.split_whitespace();
                    if let (Some(name), Some(key)) = (parts.next(), parts.next()) {
                        self.inner_map
                            .insert(name.to_string(), key.as_bytes().to_vec());
                    } else {
                        error!("error");
                    }
                }
                Err(err) => {
                    error!("can't read line");
                    return Err(Box::new(err));
                }
            }
        }
        return Ok(());
    }

    pub fn update_dictionary(&mut self, path: &Path) -> Result<(), Box<dyn std::error::Error>> {
        let display = path.display();
        let file = match File::open(path) {
            Err(ref error) if error.kind() == io::ErrorKind::NotFound => {
                match File::create(&path) {
                    Err(why) => {
                        error!("Error opening file {}: {}", display, why);
                        return Err(Box::new(why));
                    }
                    Ok(file) => file,
                }
            }
            Err(why) => {
                error!("couldn't open {}: {}", display, why);
                return Err(Box::new(why));
            }
            Ok(file) => file,
        };

        let mut buf_writer = BufWriter::new(file);
        for (name, key) in self.inner_map.iter() {
            match writeln!(buf_writer, "{} {}", name, String::from_utf8_lossy(&key)) {
                Err(why) => {
                    error!("couldn't write to {}: {}", display, why);
                    return Err(Box::new(why));
                }
                Ok(_) => {
                    println!("Successfully wrote to {}", display);
                    return Ok(());
                }
            };
        }
        return Ok(());
    }
}

impl MyKeypair {
    pub fn init() -> MyKeypair {
        MyKeypair {
            sk: Vec::new(),
            pk: Vec::new(),
        }
    }

    pub fn access_sk(&mut self) -> &Vec<u8> {
        &self.sk
    }

    pub fn generate_keypair(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        // Courbes valide par l'ANSSI: la courbe FRP256v1 – définie dans le journal officiel
        // no 241 du 16/10/2011 et dont les paramètres, validés par l’ANSSI, peuvent libre-
        // ment être intégrés dans tous les produits de sécurité – ou lorsqu’ils utilisent
        // l’une des courbes P-256, P-384, P-521, B-283, B-409 et B-571 définies dans le
        // FIPS 186-4
        // using secp256r1 curve
        let ec_group = ec::EcGroup::from_curve_name(openssl::nid::Nid::X9_62_PRIME256V1)?;
        let ec_key = ec::EcKey::generate(&ec_group)?;
        let pkey = pkey::PKey::from_ec_key(ec_key)?;
        let private_key_pem = pkey.private_key_to_pem_pkcs8().unwrap();
        let public_key_pem = pkey.public_key_to_pem().unwrap();
        // let sk = String::from_utf8(private_key_pem.clone())?; // how to not move here?
        // let pk = String::from_utf8(public_key_pem.clone())?;

        if self.pk.is_empty() {
            MyKeypair::change_keypair(self, private_key_pem, public_key_pem)?;
        } else {
            println!("Keypair already exists, replace with a new one?");
            let mut input = String::new();
            io::stdin()
                .read_line(&mut input)
                .expect("Failed to read line");
            if input == "y" {
                MyKeypair::change_keypair(self, private_key_pem, public_key_pem)?;
            } else {
                info!("Keypair not changed");
            }
        }
        return Ok(());
    }

    pub fn change_keypair(
        &mut self,
        pk: Vec<u8>,
        sk: Vec<u8>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        self.sk = sk;
        self.pk = pk;
        return Ok(());
    }
}
