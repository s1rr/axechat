use ax25::{frame, tnc};
use chrono::prelude::*;
use clap::{App, Arg, ArgMatches, SubCommand};
use key_management::MyKeypair;
use log::{error, info, warn, LevelFilter}; //debug, trace, warn
use openssl::{pkey, sha, sign};

pub mod key_management;

// FIXME changer la facon dont les cles sont handled (per file basis, can't store everything at once in a hashmap)
// FIXME finish frame decoding
// TODO Store the keys securely
// TODO Log in a separete file using simple log

struct AuthMsg {
    msg: String,
    sign: Vec<u8>,
}

impl AuthMsg {
    fn sign_message(
        msg: Vec<u8>, // &mut self.msg,
        private_key: &Vec<u8>,
    ) -> Result<Vec<u8>, openssl::error::ErrorStack> {
        let digest = sha::sha256(msg.as_slice()); // self.
        let private_key: &pkey::PKey<openssl::pkey::Private> =
            &pkey::PKey::private_key_from_pem(private_key)?;
        let mut signer = sign::Signer::new(openssl::hash::MessageDigest::sha256(), private_key)?;
        signer.update(&digest)?;
        let signature = Vec::new(); //signer.sign_to_vec()?;
        Ok(signature)
    }

    fn verify_signature(&self, public_key: &Vec<u8>) -> Result<bool, Box<dyn std::error::Error>> {
        // Calculate the SHA-256 hash of the message
        let digest = sha::sha256(self.msg.as_bytes());
        let public_key: pkey::PKey<openssl::pkey::Public> =
            pkey::PKey::public_key_from_pem(public_key.as_slice())?;

        // Create a verifier with the public key
        let mut verifier = sign::Verifier::new(openssl::hash::MessageDigest::sha256(), &public_key)
            .expect("Failed to create verifier");

        // Verify the signature
        verifier.update(&digest).expect("Failed to update verifier");
        let result = verifier
            .verify(self.sign.as_slice())
            .expect("Failed to verify signature");

        Ok(result)
    }
}

fn tnc_listen(
    tnc_addr: String,
    my_keypair: &mut MyKeypair,
) -> Result<(), Box<dyn std::error::Error>> {
    let addr = tnc_addr.parse::<tnc::TncAddress>()?; // err: AddressParseError
    let stream = tnc::Tnc::open(&addr)?; // err: TncError - OpenTnc { source: std::io::Error }

    while let Ok(frame) = stream.receive_frame() {
        println!("{}", Local::now());
        println!("{}", frame);

        // We consider that the failure to verify the signature of a message shouldn't be a termination reason
        // Either there are no signature, or the signature verification fails. We only warn.
        let (msg, signature) = decode_frame_content(frame.content)?;
        if signature.is_empty() {
            warn!("No signature provided!");
        } else {
            let authmsg = AuthMsg {
                msg: msg.to_string(),
                sign: signature,
            };

            match AuthMsg::verify_signature(&authmsg, &my_keypair.pk)? {
                true => info!("Message signature is verified"),
                false => warn!("!! Wrong signature, message is not verified !!"),
            };
        }
    }

    Ok(())
}

/// Decode the content of the frame to output the message and the
/// signature in two different variables.
/// Info a prelever sur frame.content.UnnumberedInformation.
fn decode_frame_content(
    frame_content: frame::FrameContent,
) -> Result<(String, Vec<u8>), Box<dyn std::error::Error>> {
    match frame_content {
        _ => warn!("!! Message is not authentified !!"),
    };
    Ok(("test".to_string(), Vec::new()))
}

fn tnc_send(
    my_keypair: &mut MyKeypair,
    my_addr: &mut String, // FIXME interet vu qu'il peut etre juste en statique? clone forcement?
    dest: String,
    tnc_addr: String,
    tnc_msg: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let tncaddr = tnc_addr.parse::<tnc::TncAddress>()?;
    let tnc = tnc::Tnc::open(&tncaddr)?;
    let dest = dest.parse::<frame::Address>()?;
    let msg = tnc_msg.as_bytes().to_vec();

    let authmsg: AuthMsg = AuthMsg {
        msg: String::from_utf8(msg.clone())?, // FIXME cloning here
        sign: AuthMsg::sign_message(msg, key_management::MyKeypair::access_sk(my_keypair))?,
    };

    let src = my_addr.parse::<frame::Address>()?;

    let frame = frame::Ax25Frame {
        source: src,
        destination: dest,
        route: Vec::new(),
        command_or_response: Some(frame::CommandResponse::Command),
        content: frame::FrameContent::UnnumberedInformation(frame::UnnumberedInformation {
            pid: frame::ProtocolIdentifier::None,
            info: authmsg.msg.as_bytes().to_vec(),
            poll_or_final: false,
        }),
    };

    tnc.send_frame(&frame)?;
    info!("Transmitted!");
    Ok(())
}

fn header() {
    println!("\n(>|\n  | A X E   C H A T\n  ! Authentified with ECDSA chat on AX.25 protocol - v0.1.0 2023\n");
}

fn cli() -> ArgMatches {
    App::new("Axe Chat")
        .version("v0.1.0")
        .author("s1rr")
        .about("Authentified with ECDSA chat on AX.25 protocol")
        .subcommand(
            SubCommand::with_name("listen")
                .about("Listen for incoming messages")
                .arg(Arg::with_name("address").required(true)),
        )
        .subcommand(
            SubCommand::with_name("send")
                .about("Send a message to a name")
                .arg(Arg::with_name("name").required(true))
                .arg(Arg::with_name("tnc").required(true))
                .arg(Arg::with_name("message").required(true)),
        )
        .subcommand(
            SubCommand::with_name("keys")
                .about("Manage keys")
                .subcommand(
                    SubCommand::with_name("list")
                        .about("List keys for a name")
                        .arg(Arg::with_name("name").required(true)),
                )
                .subcommand(
                    SubCommand::with_name("add")
                        .about("Add a key for a name")
                        .arg(Arg::with_name("name").required(true))
                        .arg(Arg::with_name("key").required(true)),
                )
                .subcommand(
                    SubCommand::with_name("rem")
                        .about("Remove a key from a name")
                        .arg(Arg::with_name("name").required(true))
                        .arg(Arg::with_name("key").required(true)),
                )
                .subcommand(
                    SubCommand::with_name("gen")
                        .about("Generate a new key for a name")
                        .arg(Arg::with_name("name").required(true)),
                )
                .subcommand(
                    SubCommand::with_name("default")
                        .about("Set a default key for a name")
                        .arg(Arg::with_name("name").required(true)),
                ),
        )
        .get_matches()
}

fn command_handling(
    cli: ArgMatches,
    my_keylist: &mut key_management::KeyList,
    my_keypair: &mut key_management::MyKeypair,
    my_addr: &mut String,
) -> Result<(), Box<dyn std::error::Error>> {
    match cli.subcommand() {
        Some(("listen", addr_matches)) => {
            // Listen command logic
            // No pb with unwrap here as if there are no commands,
            // the program would during the cli function execution
            let addr = addr_matches.value_of("address").unwrap();
            info!("Listening...");
            tnc_listen(addr.to_string(), my_keypair)?;
        }
        Some(("send", send_matches)) => {
            // Send command logic
            let name = send_matches.value_of("name").unwrap();
            let tnc = send_matches.value_of("tnc").unwrap();
            let message = send_matches.value_of("message").unwrap();
            info!(
                "Sending message to {} at address {}: {}",
                name, tnc, message
            );
            tnc_send(
                my_keypair,
                my_addr,
                name.to_string(),
                tnc.to_string(),
                message.to_string(),
            )?;
        }
        Some(("keys", keys_matches)) => match keys_matches.subcommand() {
            Some(("list", _)) => {
                // Keys list command logic
                info!("Listing keys: ");
            }
            Some(("add", add_matches)) => {
                // Keys add command logic
                let name = add_matches.value_of("name").unwrap();
                let key = add_matches.value_of("key").unwrap();
                info!("Adding key {} for name: {}", key, name);
                key_management::KeyList::add_key(
                    my_keylist,
                    name.to_string(),
                    key.as_bytes().to_vec(),
                )?;
            }
            Some(("rem", rem_matches)) => {
                // Keys rem command logic
                let name = rem_matches.value_of("name").unwrap();
                info!("Removing key from name: {}", name);
                key_management::KeyList::rem_key(my_keylist, name.to_string())?;
            }
            Some(("gen", gen_matches)) => {
                // Keys gen command logic
                let recipient = gen_matches.value_of("name").unwrap();
                info!("Generating new key for name: {}", recipient);
                key_management::MyKeypair::generate_keypair(my_keypair)?;
            }
            Some(("default", def_matches)) => {
                // Keys default command logic
                let default = def_matches.value_of("name").unwrap();
                info!("Setting default key for name: {}", default);
                // key_management::MyKeypair::change_keypair(my_keypair, pk, sk)
            }
            _ => error!("No or unrecognized 'keys' subcommand provided."),
        },
        None => {
            // Default command logic
            info!("Welcome, get help with --help");
            // Add your default action here
        }
        _ => error!("No or unrecognized subcommand provided."),
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::Builder::new()
        .filter_level(LevelFilter::Info)
        .init();

    header();

    let cli_res = cli(); // no error handling here

    let mut my_keylist: key_management::KeyList = key_management::KeyList::init();
    let mut my_keypair: key_management::MyKeypair = key_management::MyKeypair::init();
    let mut my_addr: String = "ok".to_string();
    // I need to primarily set variables like which is my keypair, and what is my address. Should show every time
    command_handling(cli_res, &mut my_keylist, &mut my_keypair, &mut my_addr)?;

    Ok(())
}
